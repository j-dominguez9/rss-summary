import discord

# from groq import Groq
import feedparser
from dotenv import load_dotenv
import os
from datetime import datetime, timedelta, timezone
import asyncio
import requests
from bs4 import BeautifulSoup
import openai
import smtplib
from email.message import EmailMessage


load_dotenv()


def get_content(url):
    if not url.startswith("https://"):
        url = "https://" + url
    try:
        content = requests.get(url)
        status_code = content.status_code
    except Exception as e:
        print(f"Error fetching the url: {url}")
        print(e)
    else:
        if status_code == 200:
            return content.text


def parse_site(content):
    try:
        soup = BeautifulSoup(content, "html5lib")
    except Exception as e:
        print("Could not parse xml")
        print(e)

    else:
        final_text = ""
        pars = soup.find_all("p")
        for p in pars:
            final_text += p.get_text().strip()
        return final_text


system_prompt = """\
You are an expert at creating summaries of articles. Summaries will be three to five sentences in length. Summaries should be factual, informative, fairly simple in structure, and free from exaggeration, hype, or marketing speak.

### Style: 
 - Avoid passive voice. 
 - Choose non-compound verbs whenever possible. For example. use "banned" instead of "has banned," "released" instead of "has released."
 - Avoid the words "content" and "creator."
 - Spell out "8B" as "8 billion," "100M" as "100 million," etc.
 - Abbreviate U.S. and U.K. with periods; abbreviate AI as AI (no periods).
 - Use smart quotes, not straight quotes.

The first sentence should explain what has happened in clear, simple language. The second sentence should identify important details that are relevant to an audience of AI developers. The third sentence should explain why this information matters to an audience of readers who closely follow AI news.

Give each summary a title in sentence case. The title should not repeat too many words or phrases directly from the summaries. 

Match the style in writing of the following summaries:

<summaries>
<summary index="1">
<summary_content>
Claude 3 beats GPT-4 Turbo on the Chatbot Arena leaderboard for the first time
Anthropic’s Opus took the top spot on LMSYS/HuggingFace’s AI leaderboard, as voted by thousands of chatbot users. Various versions of GPT-4 have led the Chatbot Arena board since the model’s introduction in May 2023. Claude’s smaller Sonnet and Haiku models are also performing well on the leaderboard, with Sonnet ranking just behind Gemini Pro and Haiku beating some older GPT-4 models. Researchers use the rankings to complement models’ quantitative benchmarks, amalgamating users’ qualitative perception of chatbots’ output and behavior.
</summary_content>
</summary>


<summary index="2">
<summary_content>
Adobe’s new Firefly Services offers over 20 generative AI APIs to developers
The new set of APIs enables developers to integrate AI-powered features from Adobe’s Creative Cloud, such as Photoshop, into their own custom workflows or to develop entirely new applications. Firefly Services aims to automate workflows with APIs for tasks like background removal, smart cropping, and photo leveling, and includes access to advanced Photoshop capabilities like Generative Fill and Expand. Adobe also unveiled Custom Models, a feature that lets businesses tailor Firefly models with their unique assets for even more personalized content creation
</summary_content>
</summary>

<summary index="3">
<summary_content>
Congressional aides barred from using Microsoft Copilot for government use
The U.S. House of Representatives banned Microsoft’s AI engine due to concerns it may store data on unapproved cloud servers. The move follows a similar restriction on use of ChatGPT, but in that case staffers were still allowed to use the paid version of the chatbot in limited cases. Microsoft has promised to release a version of Copilot that meets government security and compliance requirements later this year.
</summary_content>
</summary>
</summaries>

Double-check summaries to make sure they are accurate and remove any exaggerated language.
"""
fewshot1_input = """\
Please summarize the following article:\n\n<article>\nFor the last several months, a city at the heart of Silicon Valley has been training artificial intelligence to recognize tents and cars with people living inside in what experts believe is the first experiment of its kind in the United States.\nLast July, San Jose issued an open invitation to technology companies to mount cameras on a municipal vehicle that began periodically driving through the city’s district 10 in December, collecting footage of the streets and public spaces. The images are fed into computer vision software and used to train the companies’ algorithms to detect the unwanted objects, according to interviews and documents the Guardian obtained through public records requests.\nSome of the capabilities the pilot project is pursuing – such as identifying potholes and cars parked in bus lanes – are already in place in other cities. But San Jose’s foray into automated surveillance of homelessness is the first of its kind in the country, according to city officials and national housing advocates. Local outreach workers, who were previously not aware of the experiment, worry the technology will be used to punish and push out San Jose’s unhoused residents.\nCity employees are driving a single camera-equipped vehicle through sections of district 10 “every couple weeks”, said Khaled Tawfik, director of the San Jose information technology department. The city sends the training footage to participating companies, which include Ash Sensors, Sensen.AI, Xloop Digital, Blue Dome Technologies and CityRover.\nCalifornia narrowly passes Prop 1 plan aimed at tackling homelessness crisis\nRead more\nSome of the areas in district 10 targeted by the pilot, such as Santa Teresa Boulevard, are places where unhoused people congregate, sometimes with the city’s encouragement. The light rail station on Santa Teresa Boulevard, for example, is home to the city’s only designated safe parking location for RVs, often used as homes.\nThere’s no set end date for the pilot phase of the project, Tawfik said in an interview, and as the models improve he believes the target objects could expand to include lost cats and dogs, parking violations and overgrown trees.\n“If the City were to productionize this technology, we envision the cameras to be on our fleet motor pool vehicles that regularly drive throughout city limits,” a city employee wrote in a 22 January email.\nKen Salsman, chief technology officer for Ash Sensors, said his company, which specializes in sensors that monitor the structural health of buildings, had not explored homelessness detection before learning of San Jose’s pilot. The experiment provided an opportunity to create potentially marketable technologies by solving challenging computer vision problems, such as distinguishing an empty RV parked outside a home from an RV that is a home. He said the company was training its algorithms to detect proxy signs of habitation.\n“Are the windows covered inside the vehicle? Are there towels to provide privacy? Is there trash outside the vehicle, suggesting they’re using food and having trouble getting rid of the waste?” Salsman said. He added that successful detection of lived-in vehicles would probably require frequent scanning of city streets in order to establish whether the vehicles have moved.\nA report from the company Sensen.AI shows that its system detected 10 lived-in vehicles in footage collected from two streets on 8 February. Several of the vehicles pictured in the report have tarps spread across windows or rolled up and tied to them. Another has traffic cones next to it. Sensen.AI did not respond to a request for comment.\nTawfik said the goal of the pilot was to encourage companies to build algorithmic models that could detect a variety of different objects from car-mounted cameras with at least 70% accuracy. The participating companies are currently detecting lived-in RVs with between 70 and 75% accuracy, he said, but the accuracy for lived-in cars is still far lower: between 10 and 15%. City staff are following the route of the camera-equipped car and confirming that the vehicles are occupied.\n‘We’re not detecting folks. We’re detecting encampments’\nCity documents state that, in addition to accuracy, one of the main metrics the AI systems will be assessed on is their ability to preserve the privacy of people captured on camera – for example, by blurring faces and license plates. Tawfik said that the city did not “capture or retain images of individuals” through the pilot and that “the data is intended for [the city’s housing and parks departments] to provide services”.\nThe data use policy for the pilot states that the footage cannot be actively monitored for law enforcement purposes, but that police may request access to previously stored footage.\n“We’re not detecting folks,” Tawfik said. “We’re detecting encampments. So the interest is not identifying people because that will be a violation of privacy.” However, in its report identifying lived-in vehicles, Sensen.AI wrote that its system included optical character recognition of the vehicles’ license plate numbers.\nTawfik said San Jose had delayed its release of a citywide AI policy in part to allow the department to examine its proposed guardrails through the lens of the object detection pilot.\nResidents have complained to the city’s 311 phone line about homeless encampments 914 times so far in 2024. They reported illegal dumping 6,247 times, graffiti 5,666 times, and potholes 769 times last year. The goal of the surveillance pilot is to address these complaints more efficiently, according to Tawfik.\nAccording to Tawfik, the city’s response might include sending outreach workers to visit a single tent before it can grow into an encampment, he said. The San Jose housing department and non-profits providing aid to unhoused people said they had not been involved in the pilot.\n“Our ability to help the individuals directly is not really part of the pilot,” Tawfik said. “We’re still learning what can be done. And then once the program is mature, then we can look at the data and see what makes sense.”\nThat approach worries people like Thomas Knight, who was formerly unhoused and now serves as executive member of the Lived Experience Advisory Board of Silicon Valley. The group, made up of dozens of current and formerly unhoused people, has recently been fighting a policy proposed last August by the San Jose mayor, Matt Mahan, that would allow police to tow and impound lived-in vehicles near schools.\n“If their whole purpose is to better provide responses to calls to 311, then that means that this computer system is going to identify tents and lived-in vehicles that are in places that the city has deemed they shouldn’t be,” Knight said. “The truth is, the only people you’re going to be able to give [that data] to to fix the issue is the police department.”\nSan Jose is one of the least affordable housing markets in the country. In order to afford the average effective monthly rent for a one-bedroom apartment in the city, a renter would have to earn $96,000 a year, according to the latest available data. The city’s unhoused population has grown from approximately 4,200 people in 2009 to more than 6,200 in 2023. More than two-thirds of those people are living outdoors and in vehicles rather than the city’s overwhelmed shelter system.\nAmid a lack of temporary shelter beds and permanent affordable housing, San Jose officials have cracked down on tent encampments and people living in cars and RVs. Housing advocates fear that identification of encampments by roving AI would add to those efforts.\n“The approach to homelessness is to treat unhoused people as blight consistent with trash or graffiti,” said Tristia Bauman, directing attorney for housing at the non-profit Law Foundation of Silicon Valley.\nLast fall, the city cleared dozens of people out of tents and vehicles along a half-mile stretch of the downtown Guadalupe River trail and then announced plans for a “no return zone”. This year, police distributed 72-hour notices ordering people to leave a nearby encampment in Columbus Park in order to clear space for the opening of a five-acre dog park.\nSan Jose: a technological bellwether\nIn addition to providing a training ground for new algorithms, San Jose’s position as a national leader on government procurement of technology means that its experiment with surveilling encampments could influence whether and how other cities adopt similar detection systems. The city’s IT department is leading a national coalition of more than 150 municipal agencies working to develop policies for “responsible and purposeful” deployment of AI technologies in the public sector.\nTawfik said his staff had discussed the object detection pilot with coalition members and hoped that other agencies would participate in the review process. Companies participating in the pilot have also expressed interest in the “scalability” potential the coalition represents, according to emails they sent to IT department staff.\n“As we see more interest from other cities to participate, we’re sharing notes and hopefully that advances the program faster,” Tawfik said.\nKnight, from the Advisory Board, said the city’s focus on perfecting a technological solution ignored the root cause of the housing crisis in San Jose.\n“If you have no place to put people, it’s pretty much useless,” he said.\nI hope you appreciated this article. Before you move on, I wanted to ask if you would consider supporting the Guardian’s journalism as we enter one of the most consequential news cycles of our lifetimes in 2024.\nWith the potential of another Trump presidency looming, there are countless angles to cover around this year’s election – and we'll be there to shed light on each new development, with explainers, key takeaways and analysis of what it means for America, democracy and the world. \nFrom Elon Musk to the Murdochs, a small number of billionaire owners have a powerful hold on so much of the information that reaches the public about what’s happening in the world. The Guardian is different. We have no billionaire owner or shareholders to consider. Our journalism is produced to serve the public interest – not profit motives.\nAnd we avoid the trap that befalls much US media: the tendency, born of a desire to please all sides, to engage in false equivalence in the name of neutrality. We always strive to be fair. But sometimes that means calling out the lies of powerful people and institutions – and making clear how misinformation and demagoguery can damage democracy.\nFrom threats to election integrity, to the spiraling climate crisis, to complex foreign conflicts, our journalists contextualize, investigate and illuminate the critical stories of our time. As a global news organization with a robust US reporting staff, we’re able to provide a fresh, outsider perspective – one so often missing in the American media bubble.\n</article>\
"""
fewshot1_output = """\
U.S. city tests AI to identify homeless encampments \nSan Jose, California embarked on a controversial project to train AI algorithms to spot signs of homelessness, such as tents and occupied vehicles. The city teamed up with tech firms to collect street footage via a camera-mounted municipal vehicle. The initiative aims to address complaints such as illegal dumping and graffiti by enabling more efficient city responses, but has sparked concern among local outreach workers and national housing advocates about potential punitive uses against the unhoused population.\
"""


def llm_response(text):
    client = openai.OpenAI(
        base_url="https://api.together.xyz/v1",
        api_key=os.getenv("TOGETHER_API_KEY"),
    )
    # client = Groq(api_key=os.getenv("GROQ_API"))
    completion = client.chat.completions.create(
        model="meta-llama/Meta-Llama-3.1-70B-Instruct-Turbo",
        messages=[
            {"role": "system", "content": system_prompt},
            {"role": "user", "content": fewshot1_input},
            {"role": "assistant", "content": fewshot1_output},
            {
                "role": "user",
                "content": f" Please summarize the following article:\n\n<article>\n{text}\n</article>",
            },
        ],
        temperature=1,
        max_tokens=600,
        top_p=1,
        stream=False,
        stop=None,
    )
    return completion.choices[0].message.content


def send_email(text_body, url):

    msg = EmailMessage()
    txt_content = f"{text_body}\n\n"
    msg.set_content(txt_content)

    html_content = f"""
<html>
  <body>
    <h2>{txt_content.split("\n")[0].title()}</h2>
    <p>{txt_content.split("\n")[1]}</p>
    <p><a href="{url}">{url}</a></p>
  </body>
</html>        
"""

    msg.add_alternative(html_content, subtype="html")
    # me == the sender's email address
    # you == the recipient's email address
    msg["Subject"] = f"[RSS-bot] {text_body.split("\n")[0].strip()}"
    msg["From"] = os.getenv("SENDER_EMAIL")
    msg["To"] = os.getenv("RECIPIENT_EMAIL")

    # gmail
    smtp_server = "smtp.gmail.com"
    port = 587
    sender_email = os.getenv("SENDER_EMAIL")
    password = os.getenv("GOOGLE_APP")

    try:
        server = smtplib.SMTP(smtp_server, port)
        server.starttls()  # Secure the connection
        server.login(sender_email, password)
        server.send_message(msg)
        print("Email sent successfully")
    except Exception as e:
        print(f"An error occurred: {e}")
    finally:
        server.quit()


rss_url = os.getenv("RSS_FEED_URL")

# Starting Discord section
intents = discord.Intents.default()
# intents.message_content = True
# intents.messages = True

# Set Discord client
client = discord.Client(intents=intents)

now = datetime.now(timezone.utc)
time_range = timedelta(days=1)
channel_id = int(os.getenv("DISCORD_CHANNEL_ID"))
feed_entries = set()


async def check_feed():
    while True:
        feed = feedparser.parse(rss_url)
        new_entries = set(entry.link for entry in feed.entries) - feed_entries

        if new_entries:
            channel = client.get_channel(channel_id)

            for entry in feed.entries:
                if entry.link in new_entries:
                    article_url = entry.link
                    if "</p>" in entry.summary:
                        article_content = entry.summary.split("</p>")[1]
                    else:
                        article_content = entry.summary
                    article_date = datetime.strptime(
                        entry.published, "%a, %d %b %Y %H:%M:%S %z"
                    )

                    if now - article_date <= time_range:
                        content = get_content(article_url)
                        if content != None:
                            text = parse_site(content)
                            if text != None:
                                response = llm_response(text)
                                send_email(response, article_url)
                                if len(response) > 1500:
                                    response = response[:1500]
                                await channel.send(
                                    f"## {entry.title}\n[Link]({article_url})\n### Date:\n{entry.published}\n## Summary:\n{response}\n\n"
                                )
                            else:
                                await channel.send(
                                    f"## {entry.title}\n[Link]({article_url})\n### Date:\n{entry.published}\n## Summary:\n{article_content}"
                                )

            feed_entries.update(new_entries)

        await asyncio.sleep(9)


@client.event
async def on_ready():
    print(f"we have logged in as {client.user}")
    client.loop.create_task(check_feed())


client.run(os.getenv("DISCORD_TOKEN"))
